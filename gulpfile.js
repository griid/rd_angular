var gulp = require('gulp-param')(require('gulp'), process.argv);
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var minifyHTML = require('gulp-htmlmin');
var concat = require('gulp-concat');
var del = require('del');
var webpack = require('webpack-stream');
var htmlReplace = require('gulp-html-replace');
var watch = require('gulp-watch');
var sass = require('gulp-sass');
var template = require('gulp-template');
var data = require('gulp-data');
var rename = require("gulp-rename");
var spritesmith = require('gulp.spritesmith');
var tinylr;

////////////////////////////////////////////////////////////////////////////////
// GENERATE BUILD
////////////////////////////////////////////////////////////////////////////////
gulp.task('clean', function() {
  return del.sync(['dist/**']);
});

gulp.task('webpack', ['clean'], function() {
  return gulp.src('src/app/app.ts')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('src/'));
});

gulp.task('js', ['webpack'], function() {
  return gulp.src('src/bundle.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist/'));
});

gulp.task('css', function() {
  return gulp.src('src/**/*.css')
    .pipe(concat('main.css'))
    .pipe(autoprefixer())
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/'));
});

gulp.task('css-components', function() {
  return gulp.src('src/app/**/*.css')
    .pipe(gulp.dest('dist/app/'));
});

gulp.task('html', function() {
  return gulp.src('src/index.html')
    .pipe(htmlReplace({
      'css': {
        src: 'main.css',
        tpl: '<link rel="stylesheet" type="text/css" href="%s">'
      },
      'js': ['dependencies.js', 'bundle.js']
    }))
    .pipe(minifyHTML())
    .pipe(gulp.dest('dist/'));
});

gulp.task('html-components', function() {
  return gulp.src('src/app/**/*.html')
    .pipe(gulp.dest('dist/app/'));
});

gulp.task('assets', function() {
  return gulp.src('src/**/*.png')
    .pipe(gulp.dest('dist/'));
});

gulp.task('dependencies', function() {
  return gulp.src(['node_modules/reflect-metadata/Reflect.js', 'node_modules/zone.js/dist/zone.js'])
    .pipe(concat('dependencies.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/'));
});

////////////////////////////////////////////////////////////////////////////////
// COMPILE FILES/SOURCES
////////////////////////////////////////////////////////////////////////////////

gulp.task('sass', function () {
  gulp.src('src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('src/'));
});

gulp.task('sprite', function () {
  var spriteData = gulp.src('src/img/sprites/*.png').pipe(spritesmith({
    imgName: 'img/sprite.png',
    cssName: 'css/_icons.scss',
    retinaImgName: 'img/sprite@2x.png',
    retinaSrcFilter: 'src/img/sprites/*@2x.png'
  }));
  return spriteData.pipe(gulp.dest('src/'));
});
////////////////////////////////////////////////////////////////////////////////
// GENERATE NEW COMPONENT
////////////////////////////////////////////////////////////////////////////////
gulp.task('generate-html', function (component, path) {
  console.log('HTML#component', component);
  console.log('HTML#path', path);
  return gulp.src('tpl/component.html')
    .pipe(data(function () { return {name: component}; }))
    .pipe(template())
    .pipe(rename(component+'.html'))
    .pipe(gulp.dest('src/app/' + path + component + '/'));
});

gulp.task('generate-ts', function (component, path) {
  var cNameCamel = camelize(component);
  return gulp.src('tpl/component.ts')
    .pipe(data(function () { return {name: component, classname: cNameCamel, path: path}; }))
    .pipe(template())
    .pipe(rename(component+'.ts'))
    .pipe(gulp.dest('src/app/' + path + component + '/'));
});

function camelize(str) {
  return (str||'').toLowerCase().replace(/(\b|-)\w/g, function(m) {
    return m.toUpperCase().replace(/-/,'');
  });
}

gulp.task('generate-scss', function (component, path) {
  return gulp.src('tpl/component.scss')
    .pipe(data(function () { return {name: component}; }))
    .pipe(template())
    .pipe(rename(component+'.scss'))
    .pipe(gulp.dest('src/app/' + path + component + '/'));
});

gulp.task('generate', function(component, path) {
  component = component.toLowerCase();
  path = path || 'components/';
  console.log('component', component);
  console.log('path', path);
  runSequence('generate-html','generate-ts','generate-scss');
});

////////////////////////////////////////////////////////////////////////////////
// LIVE RELOAD FUNCTIONALITY
////////////////////////////////////////////////////////////////////////////////
gulp.task('livereload', function() {
  tinylr = require('tiny-lr')();
  tinylr.listen(35729);
});

function notifyLiveReload(event) {
  var fileName = require('path').relative(__dirname, event.path);

  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}

////////////////////////////////////////////////////////////////////////////////
// USER COMMANDS PARTIAL  TASKS
////////////////////////////////////////////////////////////////////////////////
gulp.task('compile', ['sprite', 'sass']);

gulp.task('watch', function() {
  gulp.watch('src/**/*.scss', ['sass']);
  gulp.watch('src/**/*.html', notifyLiveReload);
  gulp.watch('src/**/*.css', notifyLiveReload);
});

gulp.task('express', function() {
  var express = require('express');
  var app = express();
  app.use(require('connect-livereload')({port: 35729}));
  app.use('/node_modules', express.static(__dirname + '/node_modules'))
  app.use('/', express.static(__dirname + '/src'));
  app.listen(3000, '0.0.0.0');
  console.log('Server running.');
  console.log('Run localhost:3000 in your browser');
});

gulp.task('express.prod', function() {
  var express = require('express');
  var app = express();
  app.use('/', express.static(__dirname + '/dist'));
  app.listen(4000, '0.0.0.0');
  console.log('Server running.');
  console.log('Run localhost:4000 in your browser');
});
////////////////////////////////////////////////////////////////////////////////
// USER COMMANDS
////////////////////////////////////////////////////////////////////////////////
gulp.task('serve', ['compile', 'express', 'livereload', 'watch']);

gulp.task('build', ['js', 'sass', 'css-components', 'css', 'html-components', 'html', 'assets', 'dependencies'], function() {
  return del.sync(['src/bundle.js'])
});

gulp.task('serve.prod', ['express.prod']);

gulp.task('default', function(){
  console.log('Possible commands:');
  console.log('"gulp generate --component <component-name> --path <path-name>" - to generate new component');
  console.log('<component-name> should be lower case dash separated e.g. about-us ');
  console.log('<path-name> should be lower case without spaces. Must have slash at the end!');
  console.log('suggested paths: components/, pages/, services/');
  console.log('"gulp serve" - to run developer environment');
  console.log('"gulp build" - to build release version');
  console.log('"gulp serve.prod" - to run environment from released version');
});
