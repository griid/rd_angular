import {Component, OnInit} from 'angular2/core'

import {Api} from '../../services/api/api'

@Component({
    selector: 'contact',
    templateUrl: 'app/pages/contact/contact.html',
    styleUrls: ['app/pages/contact/contact.css'],
    providers: [Api]
})
export class Contact implements OnInit {
    message: string;
    data: Object[];

    constructor(private _apiService: Api) {
        var that = this;
    }

    getDataFromApi() {
        this._apiService.getData().then(function(value) {
            // fulfillment
            console.log('#OK', value);
            this.data = value
        }, function(reason) {
            // rejection
            console.log('#NOT OK');
        });
    }

    ngOnInit() {
        this.getDataFromApi();
    }

}
