import {Component} from 'angular2/core'

import {Test} from '../../test/test'
import {L10n} from '../../components/l10n/l10n';



@Component({
  selector: 'home',
  templateUrl: 'app/pages/home/home.html',
  styleUrls: ['app/pages/home/home.css'],
  directives: [Test, L10n]
})
export class Home {
  message: string;

  constructor() {
    var that = this;
  }
}
