import {Injectable} from 'angular2/core';

@Injectable()
export class Api {
  data: Object;

  constructor() {
    this.data = {};
    this.data['hello'] = 'Hello World';
    this.data['home'] = 'Home';
    this.data['contact'] = 'Contact';
  }

  getData() {
    return Promise.resolve(this.data);
  }
}
