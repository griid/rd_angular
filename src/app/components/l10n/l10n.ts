import {Component, OnInit, Input} from 'angular2/core'
import {Api} from '../../services/api/api'

@Component({
  selector: 'l10n',
  templateUrl: 'app/components/l10n/l10n.html',
  styleUrls: ['app/components/l10n/l10n.css'],
  providers: [Api]
})

// @Input() key;

export class L10n implements OnInit {
  label: string = 'ERROR';
  @Input() key: string = '';

  constructor(private _apiService: Api) {
    var that = this;
  }

  // ngOnInit() {
  //     this.label = this.key;
  //     console.log('key', this.key);
  // }

  getDataFromApi() {
      var that = this;
      this._apiService.getData().then(function(value) {
          // fulfillment
          console.log('#OK', value[that.key]);

          if(value[that.key]) {
            that.label = value[that.key];
          } else {
            that.label = '>>'+that.key+'<<';
          }

      }, function(reason) {
          // rejection
          console.log('#NOT OK');
      });
  }

  ngOnInit() {
      this.getDataFromApi();
  }
}
