import {Component} from 'angular2/core'
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router'

import {Contact} from './pages/contact/contact'
import {Home} from './pages/home/home'
import {Footer} from './components/footer/footer';
import {L10n} from './components/l10n/l10n';


@Component({
  selector: 'app',
  templateUrl: 'app/app.html',
  styleUrls: ['app/app.css'],
  directives: [ROUTER_DIRECTIVES, Footer, L10n]
})

@RouteConfig([
//   // {path:'/crisis-center', name: 'CrisisCenter', component: CrisisListComponent},
  {path:'/',        name: 'Home',       component: Home},
  {path:'/contact',      name: 'Contact',   component: Contact}
])

export class AppComponent {
  message: string;

  constructor() {
    var that = this
  }
}
